FROM archlinux:latest

MAINTAINER Vladislav Aleynikov <vladislav_v_01@mail.ru>

# ARG SDL2_VERSION=2.0
# ARG SDL2_BUILD=12

RUN pacman -Syu --noconfirm git gcc cmake make ninja wget f2fs-tools dosfstools ntfs-3g alsa-lib alsa-utils file-roller p7zip unrar gvfs aspell-ru pulseaudio glibc libglvnd libibus libx11 libxcursor libxext libxrender alsa-lib fcitx jack libpulse libxinerama libxkbcommon libxrandr libxss mesa wayland wayland-protocols

# RUN mkdir SDL2
# RUN wget -P SDL2 https://www.libsdl.org/release/SDL2-${SDL2_VERSION}.${SDL2_BUILD}.tar.gz
# RUN tar -C SDL2 -xzvf SDL2/SDL2-${SDL2_VERSION}.${SDL2_BUILD}.tar.gz > /dev/null

# RUN cd ./SDL2/SDL2-${SDL2_VERSION}.${SDL2_BUILD} && mkdir build
# RUN cd ./usr/include/dbus-1.0/dbus && ls

# RUN cd ./SDL2/SDL2-${SDL2_VERSION}.${SDL2_BUILD}/build && cmake -G"Ninja" -DCMAKE_BUILD_TYPE=Debug ..
# RUN cmake --build ./SDL2/SDL2-${SDL2_VERSION}.${SDL2_BUILD}/build
# RUN cmake --install ./SDL2/SDL2-${SDL2_VERSION}.${SDL2_BUILD}/build

#RUN /SDL2/SDL2-${SDL2_VERSION}.${SDL2_BUILD}/configure
#RUN make
#RUN make install

# RUN ls
# RUN sudo rm -rf  SDL2

ENV CMAKE_GENERATOR "Ninja"

COPY . .

CMD ./scripts/run.sh
