# FUN projects
Build platform | Status (tests only)
---------------|--------------------
MSVC 2017 x64  | [![MSVC 2017 Build](https://travis-ci.com/travis-ci/travis-web.svg?branch=master)](https://travis-ci.com/travis-ci/travis-web)
Linux x64      | [![Linux x64](https://travis-ci.com/travis-ci/travis-web.svg?branch=master)](https://travis-ci.com/travis-ci/travis-web)

# Used technologies
These technologies use in my projects and are necessary for their work:
- [CMake](https://www.cmake.org/) v17+
- [GCC](https://gcc.gnu.org/) v9.3.0+
- [Ninja](https://ninja-build.org/) v1.10.0+
- [SDL2](http://libsdl.org/) v2.0.12+
- [Make](https://www.gnu.org/software/make/) v4.3
- Standart C++17 or higher

# Installation
### Installation on Arch Linux
- To install [CMake](https://www.cmake.org/), enter next commands in the trminal:
```sh
$ sudo pacman -S --noconfirm cmake
```
- To install [GCC](https://gcc.gnu.org/), enter next commands in the trminal:
```sh
$ sudo pacman -S --noconfirme gcc
```
- To install [Ninja](https://ninja-build.org/), enter next commands in the trminal:
```sh
$ sudo pacman -S --noconfirme ninja
```
- To install [Make](https://www.gnu.org/software/make/), enter next commands in the trminal:
```sh
$ sudo pacman -S --noconfirm make
```
- To install [SDL2](http://libsdl.org/), you should install the dependencies for this library.
For this enter next commands in the terminal:
```sh
$ sudo pacman -S glibc libglvnd libibus libx11 libxcursor libxext libxrender 
alsa-lib jack libpulse alsa-lib cmake fcitx ibus jack libpulse libxinerama 
libxkbcommon libxrandr libxss mesa wayland wayland-protocols
```
- After you should dowland [SDL2-2.0.12.tar.gz](http://www.libsdl.org/release/SDL2-2.0.12.tar.gz)
from the official site [SDL2 v2.0.12](http://www.libsdl.org/download-2.0.php) (or newer version, if it is),
and unzip the dowlanded archive to the folder.
- Than go to the terminal, find the folder into which you inzipped the archive [SDL2-2.0.12.tar.gz](http://www.libsdl.org/release/SDL2-2.0.12.tar.gz)
and enter next commands:
```sh
$ mkdir build
$ cd build
$ ../configure
$ make
$ sudo make install
```
- And to install [SDL2](http://libsdl.org/) you can install package:
```$ sudo pacman -Syu sdl2```
### Generate Docker image (for GitLab pipelines)
- Write Dockerfile
- enter ```sudo systemctl start docker```
- enter ```sudo docker build -t maxmopka/game_dev_dockerfil .```
- if you want to build projects locally, enter: ```sudo docker run maxmopka/game_dev_dockerfile```
- enter ```sudo docker push maxmopka/game_dev_dockerfile```
Dockerfile content:
```sh
FROM archlinux:latest

MAINTAINER Vladislav Aleynikov <vladislav_v_01@mail.ru>

RUN pacman -Syu --noconfirm git
RUN pacman -Syu --noconfirm gcc
RUN pacman -Syu --noconfirm cmake
RUN pacman -Syu --noconfirm make
RUN pacman -Syu --noconfirm ninja
RUN pacman -Syu --noconfirm sdl2

ENV CMAKE_GENERATOR "Ninja"

COPY . .

CMD ./scripts/run.sh
```
