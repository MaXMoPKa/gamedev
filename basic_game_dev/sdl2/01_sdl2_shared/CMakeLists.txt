cmake_minimum_required(VERSION 3.17)
set(CMAKE_CXX_STANDARD 17)

project(01_sdl2_shared CXX)
add_executable(01_sdl2_shared_app main.cpp)

find_package(SDL2 REQUIRED)
find_library(SDL2_LIB libSDL2-2.0.so.0.12.0)
target_include_directories(01_sdl2_shared_app PRIVATE /usr/include/SDL2)
target_link_libraries(01_sdl2_shared_app PRIVATE libSDL2-2.0.so.0.12.0)
if(NOT SDL2_LIB)
    message(FATAL_ERROR "Error: find_library(...) did not find libSDL2-2.0.so.0.12.0")
else()
    message(STATUS "fulfilled the project ${PROJECT_NAME}")
    message(STATUS "path to shared libSDL2-2.0.so.0.12.0 [${SDL2_LIB}]")
endif()
